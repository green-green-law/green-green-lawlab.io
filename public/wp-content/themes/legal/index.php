<?php

/*

*Template Name: My Home

*/ ;?>
<!DOCTYPE html>
<!-- saved from url=(0030)http://legal.fasterthemes.com/ -->
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="http://legal.fasterthemes.com/xmlrpc.php">
        <!--[if lt IE 9]>
                <script src="http://legal.fasterthemes.com/wp-content/themes/legal/js/html5.js"></script>
        <![endif]-->
                    <link rel="shortcut icon" href="http://legal.fasterthemes.com/wp-content/uploads/2015/03/favicon.png">
                <title>Bankruptcy | Green & Green Law Firm</title>
<link rel="alternate" type="application/rss+xml" title="Legal » Feed" href="http://legal.fasterthemes.com/feed/">
<link rel="alternate" type="application/rss+xml" title="Legal » Comments Feed" href="http://legal.fasterthemes.com/comments/feed/">
<link rel="alternate" type="application/rss+xml" title="Legal » home Comments Feed" href="http://legal.fasterthemes.com/home/feed/">
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/legal.fasterthemes.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.6"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script><script src="./sample1_files/wp-emoji-release.min.js" type="text/javascript"></script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" id="fastbook-custom-css" href="./sample1_files/custom.css" type="text/css" media="all">
<link rel="stylesheet" id="fastbook-datetimepicker-css" href="./sample1_files/jquery.datetime.css" type="text/css" media="all">
<link rel="stylesheet" id="legal-bootstrap-css" href="./sample1_files/bootstrap.css" type="text/css" media="all">
<link rel="stylesheet" id="legal-font-awesome-css" href="./sample1_files/font-awesome.css" type="text/css" media="all">
<link rel="stylesheet" id="legal-style-css" href="./sample1_files/style.css" type="text/css" media="all">
<link rel="stylesheet" id="legal-owl.carousel-css" href="./sample1_files/owl.carousel.css" type="text/css" media="all">
<script type="text/javascript" src="./sample1_files/jquery.js"></script>
<script type="text/javascript" src="./sample1_files/jquery-migrate.min.js"></script>
<script type="text/javascript" src="./sample1_files/jquery.datetimepicker.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var fastbook_ajax = {"ajax_url":"http:\/\/legal.fasterthemes.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript" src="./sample1_files/validation.js"></script>
<script type="text/javascript" src="./sample1_files/bootstrap.js"></script>
<script type="text/javascript" src="./sample1_files/deafult.js"></script>
<script type="text/javascript" src="./sample1_files/masonry.min.js"></script>
<script type="text/javascript" src="./sample1_files/jquery.masonry.min.js"></script>
<script type="text/javascript" src="./sample1_files/base.js"></script>
<script type="text/javascript" src="./sample1_files/owl.carousel.js"></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://legal.fasterthemes.com/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://legal.fasterthemes.com/wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 4.3.6">
<META content="http://seal-boise.bbb.org/seals/blue-seal-280-80-green-green-law-firm-1000017988.png" 
property="og:image"> 
<link rel="canonical" href="http://legal.fasterthemes.com/">
<link rel="shortlink" href="http://legal.fasterthemes.com/">
<style> .home-image-section { background :url('http://legal.fasterthemes.com/wp-content/uploads/2015/03/legalhelp-bg-compressor.jpg'); } </style>	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    </head>
    <body class="home page page-id-5 page-template page-template-page-template page-template-front-page page-template-page-templatefront-page-php">
        <header style="position: relative;">
                            <div class="top-header" style="display: block;">
                    <div class="container theme-container"> 
                        <div class="col-md-6 col-sm-7 social-part">
                             <ul>
                                                                    <li><a href="https://www.facebook.com/Green-Green-Law-Firm-356462491112938/"><i class="fa fa-facebook"></i></a></li>
                                 
                                                                    <li><a href="https://plus.google.com/u/0/110254761377737789902"><i class="fa fa-google-plus"></i></a></li>
                                 
                            </ul>                               
                        </div>
                        <div class="col-md-6 col-sm-5 callus-part">
                            <p>
                                <span>call us now :</span>                                 (208) 898-9978                            </p>     
                        </div>
                    </div>
                </div>
                        <div class="bottom-header">
                <div class="container theme-container">
                    <div class="col-md-3 theme-logo">
                                                    <a href="http://green-green-law.com/"><img class="img-responsive" alt="Green & Green Law logo" src="index_files/logo123-compressor.png"></a>
                                                    <!--<a title="Green &amp; Green Law Firm BBB Business Review" href="http://www.bbb.org/snakeriver/business-reviews/attorneys-and-lawyers/green-green-law-firm-in-meridian-id-1000017988/#bbbonlineclick" target="_blank"><img style="border: 0;" alt="Green &amp; Green Law Firm BBB Business Review" src="http://green-green-law.com/wp-content/uploads/2017/03/AB_logo_270x103.png" scale="0"></a>-->
                                            </div>
                    <div class="col-md-9 header-navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle navbar-toggle-top sort-menu-icon collapsed" data-toggle="collapse" data-target=".navbar-collapse"> 
                                <span class="sr-only"></span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div id="example-navbar-collapse" class="theme-nav navbar-collapse collapse"><ul id="menu-top-menu" class="menu"><li id="menu-item-25" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-5 current_page_item menu-item-25"><a href="http://green-green-law.com/">home</a></li>
<li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="http://green-green-law.com/profile/">profile</a></li>
<li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-22"><a href="http://green-green-law.com/">locations</a>
<ul class="sub-menu">
	<li id="menu-item-24" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24"><a href="http://green-green-law.com/bankruptcy-meridian-id/">meridian</a></li>
	<li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23"><a href="http://green-green-law.com/bankruptcy-meridian-id/">boise</a></li>
	<li id="menu-item-45" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://green-green-law.com/bankruptcy-meridian-id/">cascade</a></li>
    <li id="menu-item-54" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://green-green-law.com/bankruptcy-meridian-id/">nampa</a></li>
    <li id="menu-item-46" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://green-green-law.com/bankruptcy-meridian-id/">eagle</a></li>
    <li id="menu-item-47" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://green-green-law.com/bankruptcy-meridian-id/">emmett</a></li>
    <li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://green-green-law.com/bankruptcy-meridian-id/">kuna</a></li>
    <li id="menu-item-49" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://green-green-law.com/bankruptcy-meridian-id/">middleton</a></li>
    <li id="menu-item-50" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://green-green-law.com/bankruptcy-meridian-id/">payette</a></li>
    <li id="menu-item-51" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://green-green-law.com/bankruptcy-meridian-id/">sand hollow</a></li>
    <li id="menu-item-52" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://green-green-law.com/bankruptcy-meridian-id/">star</a></li>
</ul>
</li>
<li id="menu-item-49" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-49"><a href="http://green-green-law.com/contact/">contact</a></li>
</ul></div> 
                    </div>    
                </div>
            </div>
        </header>
        <!--section start-->
<section class="section-main home-page" style="margin-top: 0px;">
    <div class="theme-banner">
        <div id="myCarousel" class="carousel slide carousel-fade">
            <div class="carousel-inner">
                                      
                        <div class="item active">
                            <img src="./sample1_files/slide1-compressor.jpg" alt="1">
                            <div class="blur-effect"></div>
                            <div class="banner-inner-content">
                                                                    <p><img src="./awards/prof-award.png" alt="Professionalism Award" style="width:15%;height:17%;"></p>
                                                                    <h1>J. Bart Green</h1>
                                                                    <h1>Green & Green Law Firm</h1>
                                                                    <div class="banner-button">
                                        <a href="http://green-green-law.com/contact/">
                                               call today                                        </a>
                                    </div>
                                                            </div>
                        </div>
                    
                                      
                        <div class="item">
                            <img src="./sample1_files/slide1-compressor.jpg" alt="2">
                            <div class="blur-effect"></div>
                            <div class="banner-inner-content">
                                                                    <i class="fa fa-book"></i>
                                
                                                                    <h1>Lorem Ipsum is simply dummy</h1>
                                
                                                                    <p>when an unknown printer took a galley of</p>
                                
                                                                    <div class="banner-button">
                                        <a href="http://fasterthemes.com/wordpress-themes/legal">
                                               download                                        </a>
                                    </div>
                                                            </div>
                        </div>
                    
                            </div>
            <!-- Carousel nav -->
                                <a class="carousel-control left nav-left" href="http://legal.fasterthemes.com/#myCarousel" data-slide="prev"></a>
                    <a class="carousel-control right nav-right" href="http://legal.fasterthemes.com/#myCarousel" data-slide="next"></a>
                        </div>
                        <div class="home-title">
                        <p>James B. Green was of counsel now in memoriam.</p>
                        </div>
                        <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <!-- old seal BOT
                    <a title="Green &amp; Green Law Firm BBB Business Review" href="http://www.bbb.org/snakeriver/business-reviews/attorneys-and-lawyers/green-green-law-firm-in-meridian-id-1000017988/#bbbonlineclick" target="_blank"><img class="aligncenter" style="border: 0;" src="./awards/blue-seal-200-130-green-green-law-firm-1000017988.png" alt="Green &amp; Green Law Firm BBB Business Review" scale="0"></a>
    old seal -->
    <P style="text-align: center;">&nbsp;<A title="Green &amp; Green Law Firm BBB Business Review" 
href="http://www.bbb.org/snakeriver/business-reviews/attorneys-and-lawyers/green-green-law-firm-in-meridian-id-1000017988/#bbbonlineclick" 
target="_blank"><IMG style="border: 0px currentColor; border-image: none;" alt="Green &amp; Green Law Firm BBB Business Review" 
src="http://seal-boise.bbb.org/seals/blue-seal-200-130-green-green-law-firm-1000017988.png"></A></P>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <!--</div>bot-edited-this-->
    <!--banner end-->
    <!--our best-->
    <div class="home-practise-section">
        <div class="container theme-container">
                            <div class="home-title">
                                            <h2><span>doing the best for you</span></h2>
                                                                <p><img src="./awards/awards-1-240x300.png" alt="Bankruptcy Meridian Idaho" style="float:left;width:48px;height:60px;">PROFESSIONALISM AWARD – Recipient of the Professionalism Award from the Commercial Law and Bankruptcy Section of the Idaho State Bar. - 2017</p>
                                                                <p><img src="./awards/awards-2-240x300.png" alt="Bankruptcy Meridian Idaho" style="float:right;width:48px;height:60px;">"Very Highly Rated in Both Legal Ability and Ethical Standards - Notable" in peer review by Martindale - Hubbell. - 2017</p>
                                                                <p><img src="./awards/awards-3-240x300.png" alt="Bankruptcy Nampa Idaho" style="float:left;width:48px;height:60px;">TOP 10 ATTORNEY AWARD – Named one of the ten best bankruptcy attorneys in the state of Idaho by the National Academy for Bankruptcy Attorneys. - 2014</p>
                                                                <p><img src="./awards/awards-4-240x300.png" alt="Bankruptcy Meridian Idaho" style="float:right;width:48px;height:60px;">TOP 100 ATTORNEYS IN IDAHO - Named one of the top 100 attorneys in the state of Idaho - lifetime achievement award from America's Top 100 Attorneys - 2017</p>
                                    </div>
                                    <div class="row section-column">
                                    <div class="col-sm-6 col-md-6 column-company">

                        <div class="home-title">
                            <h2><span>Idaho Bankruptcy and Estate Planning</span></h2>
                        </div>
                        <div class="company-col1">
                            <ul>
                                                                                                            <li class="col-md-6 col-sm-12">
                                            <i class="fa fa-shield"></i>
                                            <span>bankruptcy</span>
                                        </li>
                                                                                                                                                <li class="col-md-6 col-sm-12">
                                            <i class="fa fa-user"></i>
                                            <span>client-centered focus</span>
                                        </li>
                                                                                                                                                <li class="col-md-6 col-sm-12">
                                            <i class="fa fa-legal"></i>
                                            <span>individuals & businesses</span>
                                        </li>
                                                                                                                                                <li class="col-md-6 col-sm-12">
                                            <i class="fa fa-book"></i>
                                            <span>chapter 7 & chapter 13</span>
                                        </li>
                                                                                                                                                <li class="col-md-6 col-sm-12">
                                            <i class="fa fa-bullhorn"></i>
                                            <span>experienced staff</span>
                                        </li>
                                                                                                                                                <li class="col-md-6 col-sm-12">
                                            <i class="fa fa-cogs"></i>
                                            <span>Estate Planning</span>
                                        </li>
                                                                                                                                                                                                                                        </ul>
                                                            <p>We help people seeking protection from their creditors through the bankruptcy process. We take a client-centered focus in our practice of law. Service to our clients is our top priority. We explain the complex aspects of law in plain, understandable English.</p>
                                                    </div>
                    </div>
                                                    <div class="col-md-6 col-sm-6 column-book">
                        <div class="row"> <div class="panel panel-default subscribe-box col-md-6">
<h3 class="Label-book">Book an appointment</h3>
<h3 class="Label-book">---</h3>
<div class="panel-body">

                                                            <p>We seek to provide our clients with bankruptcy and estate planning legal services in a personal and professional manner. Bart Green has over 29 years of legal experience.</p>
                                                            <div class="company-col1">
                            <ul>
                                                                                                            <li class="col-md-6 col-sm-12">
                                            <i class="fa fa-book"></i>
                                            <span>probate</span>
                                        </li>
                                                                                                                                                <li class="col-md-6 col-sm-12">
                                            <i class="fa fa-user"></i>
                                            <span>related services</span>
                                        </li>
                                                                                                                                                
                                                                                                                                                                                                                                        </ul>
                                                    </div>		
        </div>
        </div>
<script type="text/javascript">
jQuery(document).ready(function($){ 
    jQuery('#datetime').datetimepicker({ 
     minDate:'0',
    });
});
</script>

</div>    
                    </div>
                            </div>
                    </div>
    </div>    
    </div>
    <!--end-->
    <!--home section image bg-->
            <div class="home-image-section">
            <div class="blur-effect">
                <div class="container theme-container">
                                            <h2>we are here to provide legal help</h2>
                                                                <div class="banner-button">
                            <a href="http://green-green-law.com/contact/">free case evaluation</a>
                        </div>
                                    </div>
            </div>     
        </div>
        <!--end-->
    <!--our section strat-->
    <div class="home-our-blog">
        <div class="container theme-container">
            <div class="home-title">
                                    <h2><span>our featured services</span></h2>
                                            </div>
            <div class="masonry-container masonry" style="position: relative; height: 516.406px;">
                <div class="row our-latest-blog masonry-brick">
                                                                        <div class="col-md-4 col-sm-6 our-latest-box" style="position: absolute; width: 370px; left: 0px; top: 0px;">
                                <div class="latest-blog-img">
                                    <div class="inner-grid">
                                                                                <a href="http://green-green-law.com/bankruptcy-meridian-id/"><img class="img-responsive" src="./sample1_files/legalhelp-bg-compressor-420x247.jpg" width="420" height="247" alt="Using Brainwriting For Rapid Idea"></a>
                                    </div>
                                    <div class="latest-blog-inner">
                                        <a class="latest-blog-title" href="http://green-green-law.com/bankruptcy-meridian-id/">Creative Bankruptcy Solutions</a>
                                        <p>We place emphasis on finding creative, but sound, solutions within the bankruptcy laws. We can: stop a foreclosure on your home, stop a lawsuit, stop garnishment of your wages/attachments of your property.
</p><div class="button-div"><a href="http://green-green-law.com/bankruptcy-meridian-id/" class="button-read">Read more</a></div>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-md-4 col-sm-6 our-latest-box" style="position: absolute; width: 370px; left: 400px; top: 0px;">
                                <div class="latest-blog-img">
                                    <div class="inner-grid">
                                                                                <a href="http://green-green-law.com/bankruptcy-meridian-id/"><img class="img-responsive" src="./sample1_files/blog-img-1-compressor-420x247.jpg" width="420" height="247" alt="Project Buildings"></a>
                                    </div>
                                    <div class="latest-blog-inner">
                                        <a class="latest-blog-title" href="http://green-green-law.com/bankruptcy-meridian-id/">We Focus On You</a>
                                        <p>We will setup an in person meeting with you at our office. We will sit down with you and go over your financial situation in detail. We will evaluate the issues you are facing and make recommendations to you on how best to address your situation.
</p><div class="button-div"><a href="http://green-green-law.com/bankruptcy-meridian-id/" class="button-read">Read more</a></div>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-md-4 col-sm-6 our-latest-box" style="position: absolute; width: 370px; left: 800px; top: 0px;">
                                <div class="latest-blog-img">
                                    <div class="inner-grid">
                                                                                <a href="http://green-green-law.com/bankruptcy-meridian-id/"><img class="img-responsive" src="./sample1_files/blog-img-2-compressor-420x247.jpg" width="420" height="247" alt="Strategy First"></a>
                                    </div>
                                    <div class="latest-blog-inner">
                                        <a class="latest-blog-title" href="http://green-green-law.com/bankruptcy-meridian-id/">Chapter 7 and Chapter 13</a>
                                        <p> Losing a job, domestic/health problems, or just being overextended, debt collectors can make life a real pain. If you are facing foreclosure, garnishment or attachment, or you just can’t pay all of the bills and taxes filing for bankruptcy will stop the harassment.
</p><div class="button-div"><a href="http://green-green-law.com/bankruptcy-meridian-id/" class="button-read">Read more</a></div>
                                    </div>
                                </div>
                            </div>
                                                            </div>
            </div>
        </div>
    </div>
    <!--end-->
</section>
<!--section end-->
<footer class="page-footer">
    <div class="container theme-container">
        <div class="footer-logo">
            <a href="http://green-green-law.com/profile/">Your Bankruptcy Attorney</a>
        </div>
                    <div class="social-widget">
                <ul>
                                            <li><a href="https://www.facebook.com/Green-Green-Law-Firm-356462491112938/" class="twitter-icon"><i class="fa fa-facebook"></i></a></li>
                     
                                            <li><a href="https://plus.google.com/u/0/110254761377737789902" class="twitter-icon"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
            </div> 
                <div class="footer-bottom">
        </div>
    </div>
</footer>
<script type="text/javascript" src="./sample1_files/comment-reply.min.js"></script>


<div class="xdsoft_datetimepicker xdsoft_noselect "><div class="xdsoft_datepicker active"><div class="xdsoft_mounthpicker"><button type="button" class="xdsoft_prev"></button><button type="button" class="xdsoft_today_button" style="visibility: visible;"></button><div class="xdsoft_label xdsoft_month"><span>September</span><div class="xdsoft_select xdsoft_monthselect xdsoft_scroller_box"><div style="margin-top: 0px;"><div class="xdsoft_option " data-value="0">January</div><div class="xdsoft_option " data-value="1">February</div><div class="xdsoft_option " data-value="2">March</div><div class="xdsoft_option " data-value="3">April</div><div class="xdsoft_option " data-value="4">May</div><div class="xdsoft_option " data-value="5">June</div><div class="xdsoft_option " data-value="6">July</div><div class="xdsoft_option " data-value="7">August</div><div class="xdsoft_option xdsoft_current" data-value="8">September</div><div class="xdsoft_option " data-value="9">October</div><div class="xdsoft_option " data-value="10">November</div><div class="xdsoft_option " data-value="11">December</div></div><div class="xdsoft_scrollbar"><div class="xdsoft_scroller" style="display: block; height: 10px; margin-top: 0px;"></div></div></div></div><div class="xdsoft_label xdsoft_year"><span>2016</span><div class="xdsoft_select xdsoft_yearselect xdsoft_scroller_box"><div style="margin-top: 0px;"><div class="xdsoft_option " data-value="1950">1950</div><div class="xdsoft_option " data-value="1951">1951</div><div class="xdsoft_option " data-value="1952">1952</div><div class="xdsoft_option " data-value="1953">1953</div><div class="xdsoft_option " data-value="1954">1954</div><div class="xdsoft_option " data-value="1955">1955</div><div class="xdsoft_option " data-value="1956">1956</div><div class="xdsoft_option " data-value="1957">1957</div><div class="xdsoft_option " data-value="1958">1958</div><div class="xdsoft_option " data-value="1959">1959</div><div class="xdsoft_option " data-value="1960">1960</div><div class="xdsoft_option " data-value="1961">1961</div><div class="xdsoft_option " data-value="1962">1962</div><div class="xdsoft_option " data-value="1963">1963</div><div class="xdsoft_option " data-value="1964">1964</div><div class="xdsoft_option " data-value="1965">1965</div><div class="xdsoft_option " data-value="1966">1966</div><div class="xdsoft_option " data-value="1967">1967</div><div class="xdsoft_option " data-value="1968">1968</div><div class="xdsoft_option " data-value="1969">1969</div><div class="xdsoft_option " data-value="1970">1970</div><div class="xdsoft_option " data-value="1971">1971</div><div class="xdsoft_option " data-value="1972">1972</div><div class="xdsoft_option " data-value="1973">1973</div><div class="xdsoft_option " data-value="1974">1974</div><div class="xdsoft_option " data-value="1975">1975</div><div class="xdsoft_option " data-value="1976">1976</div><div class="xdsoft_option " data-value="1977">1977</div><div class="xdsoft_option " data-value="1978">1978</div><div class="xdsoft_option " data-value="1979">1979</div><div class="xdsoft_option " data-value="1980">1980</div><div class="xdsoft_option " data-value="1981">1981</div><div class="xdsoft_option " data-value="1982">1982</div><div class="xdsoft_option " data-value="1983">1983</div><div class="xdsoft_option " data-value="1984">1984</div><div class="xdsoft_option " data-value="1985">1985</div><div class="xdsoft_option " data-value="1986">1986</div><div class="xdsoft_option " data-value="1987">1987</div><div class="xdsoft_option " data-value="1988">1988</div><div class="xdsoft_option " data-value="1989">1989</div><div class="xdsoft_option " data-value="1990">1990</div><div class="xdsoft_option " data-value="1991">1991</div><div class="xdsoft_option " data-value="1992">1992</div><div class="xdsoft_option " data-value="1993">1993</div><div class="xdsoft_option " data-value="1994">1994</div><div class="xdsoft_option " data-value="1995">1995</div><div class="xdsoft_option " data-value="1996">1996</div><div class="xdsoft_option " data-value="1997">1997</div><div class="xdsoft_option " data-value="1998">1998</div><div class="xdsoft_option " data-value="1999">1999</div><div class="xdsoft_option " data-value="2000">2000</div><div class="xdsoft_option " data-value="2001">2001</div><div class="xdsoft_option " data-value="2002">2002</div><div class="xdsoft_option " data-value="2003">2003</div><div class="xdsoft_option " data-value="2004">2004</div><div class="xdsoft_option " data-value="2005">2005</div><div class="xdsoft_option " data-value="2006">2006</div><div class="xdsoft_option " data-value="2007">2007</div><div class="xdsoft_option " data-value="2008">2008</div><div class="xdsoft_option " data-value="2009">2009</div><div class="xdsoft_option " data-value="2010">2010</div><div class="xdsoft_option " data-value="2011">2011</div><div class="xdsoft_option " data-value="2012">2012</div><div class="xdsoft_option " data-value="2013">2013</div><div class="xdsoft_option " data-value="2014">2014</div><div class="xdsoft_option " data-value="2015">2015</div><div class="xdsoft_option xdsoft_current" data-value="2016">2016</div><div class="xdsoft_option " data-value="2017">2017</div><div class="xdsoft_option " data-value="2018">2018</div><div class="xdsoft_option " data-value="2019">2019</div><div class="xdsoft_option " data-value="2020">2020</div><div class="xdsoft_option " data-value="2021">2021</div><div class="xdsoft_option " data-value="2022">2022</div><div class="xdsoft_option " data-value="2023">2023</div><div class="xdsoft_option " data-value="2024">2024</div><div class="xdsoft_option " data-value="2025">2025</div><div class="xdsoft_option " data-value="2026">2026</div><div class="xdsoft_option " data-value="2027">2027</div><div class="xdsoft_option " data-value="2028">2028</div><div class="xdsoft_option " data-value="2029">2029</div><div class="xdsoft_option " data-value="2030">2030</div><div class="xdsoft_option " data-value="2031">2031</div><div class="xdsoft_option " data-value="2032">2032</div><div class="xdsoft_option " data-value="2033">2033</div><div class="xdsoft_option " data-value="2034">2034</div><div class="xdsoft_option " data-value="2035">2035</div><div class="xdsoft_option " data-value="2036">2036</div><div class="xdsoft_option " data-value="2037">2037</div><div class="xdsoft_option " data-value="2038">2038</div><div class="xdsoft_option " data-value="2039">2039</div><div class="xdsoft_option " data-value="2040">2040</div><div class="xdsoft_option " data-value="2041">2041</div><div class="xdsoft_option " data-value="2042">2042</div><div class="xdsoft_option " data-value="2043">2043</div><div class="xdsoft_option " data-value="2044">2044</div><div class="xdsoft_option " data-value="2045">2045</div><div class="xdsoft_option " data-value="2046">2046</div><div class="xdsoft_option " data-value="2047">2047</div><div class="xdsoft_option " data-value="2048">2048</div><div class="xdsoft_option " data-value="2049">2049</div><div class="xdsoft_option " data-value="2050">2050</div></div><div class="xdsoft_scrollbar"><div class="xdsoft_scroller" style="display: block; height: 10px; margin-top: 0px;"></div></div></div></div><button type="button" class="xdsoft_next"></button></div><div class="xdsoft_calendar"><table><thead><tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr></thead><tbody><tr><td data-date="28" data-month="7" data-year="2016" class="xdsoft_date xdsoft_day_of_week0 xdsoft_date xdsoft_disabled xdsoft_other_month xdsoft_weekend"><div>28</div></td><td data-date="29" data-month="7" data-year="2016" class="xdsoft_date xdsoft_day_of_week1 xdsoft_date xdsoft_disabled xdsoft_other_month"><div>29</div></td><td data-date="30" data-month="7" data-year="2016" class="xdsoft_date xdsoft_day_of_week2 xdsoft_date xdsoft_disabled xdsoft_other_month"><div>30</div></td><td data-date="31" data-month="7" data-year="2016" class="xdsoft_date xdsoft_day_of_week3 xdsoft_date xdsoft_disabled xdsoft_other_month"><div>31</div></td><td data-date="1" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week4 xdsoft_date xdsoft_disabled"><div>1</div></td><td data-date="2" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week5 xdsoft_date xdsoft_disabled"><div>2</div></td><td data-date="3" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week6 xdsoft_date xdsoft_disabled xdsoft_weekend"><div>3</div></td></tr><tr><td data-date="4" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week0 xdsoft_date xdsoft_disabled xdsoft_weekend"><div>4</div></td><td data-date="5" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week1 xdsoft_date xdsoft_disabled"><div>5</div></td><td data-date="6" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week2 xdsoft_date xdsoft_disabled"><div>6</div></td><td data-date="7" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week3 xdsoft_date xdsoft_disabled"><div>7</div></td><td data-date="8" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week4 xdsoft_date xdsoft_disabled"><div>8</div></td><td data-date="9" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week5 xdsoft_date xdsoft_disabled"><div>9</div></td><td data-date="10" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week6 xdsoft_date xdsoft_disabled xdsoft_weekend"><div>10</div></td></tr><tr><td data-date="11" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week0 xdsoft_date xdsoft_disabled xdsoft_weekend"><div>11</div></td><td data-date="12" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week1 xdsoft_date xdsoft_disabled"><div>12</div></td><td data-date="13" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week2 xdsoft_date xdsoft_disabled"><div>13</div></td><td data-date="14" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week3 xdsoft_date xdsoft_disabled"><div>14</div></td><td data-date="15" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week4 xdsoft_date xdsoft_disabled"><div>15</div></td><td data-date="16" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week5 xdsoft_date xdsoft_disabled"><div>16</div></td><td data-date="17" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week6 xdsoft_date xdsoft_disabled xdsoft_weekend"><div>17</div></td></tr><tr><td data-date="18" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week0 xdsoft_date xdsoft_disabled xdsoft_weekend"><div>18</div></td><td data-date="19" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week1 xdsoft_date xdsoft_disabled"><div>19</div></td><td data-date="20" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week2 xdsoft_date xdsoft_disabled"><div>20</div></td><td data-date="21" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week3 xdsoft_date xdsoft_current xdsoft_today"><div>21</div></td><td data-date="22" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week4 xdsoft_date"><div>22</div></td><td data-date="23" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week5 xdsoft_date"><div>23</div></td><td data-date="24" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week6 xdsoft_date xdsoft_weekend"><div>24</div></td></tr><tr><td data-date="25" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week0 xdsoft_date xdsoft_weekend"><div>25</div></td><td data-date="26" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week1 xdsoft_date"><div>26</div></td><td data-date="27" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week2 xdsoft_date"><div>27</div></td><td data-date="28" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week3 xdsoft_date"><div>28</div></td><td data-date="29" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week4 xdsoft_date"><div>29</div></td><td data-date="30" data-month="8" data-year="2016" class="xdsoft_date xdsoft_day_of_week5 xdsoft_date"><div>30</div></td><td data-date="1" data-month="9" data-year="2016" class="xdsoft_date xdsoft_day_of_week6 xdsoft_date xdsoft_other_month xdsoft_weekend"><div>1</div></td></tr></tbody></table></div></div><div class="xdsoft_timepicker active"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_time_box xdsoft_scroller_box"><div class="xdsoft_time_variant" style="margin-top: 0px;"><div class="xdsoft_time " data-hour="0" data-minute="0">00:00</div><div class="xdsoft_time " data-hour="1" data-minute="0">01:00</div><div class="xdsoft_time " data-hour="2" data-minute="0">02:00</div><div class="xdsoft_time " data-hour="3" data-minute="0">03:00</div><div class="xdsoft_time " data-hour="4" data-minute="0">04:00</div><div class="xdsoft_time " data-hour="5" data-minute="0">05:00</div><div class="xdsoft_time " data-hour="6" data-minute="0">06:00</div><div class="xdsoft_time " data-hour="7" data-minute="0">07:00</div><div class="xdsoft_time " data-hour="8" data-minute="0">08:00</div><div class="xdsoft_time " data-hour="9" data-minute="0">09:00</div><div class="xdsoft_time " data-hour="10" data-minute="0">10:00</div><div class="xdsoft_time " data-hour="11" data-minute="0">11:00</div><div class="xdsoft_time " data-hour="12" data-minute="0">12:00</div><div class="xdsoft_time " data-hour="13" data-minute="0">13:00</div><div class="xdsoft_time " data-hour="14" data-minute="0">14:00</div><div class="xdsoft_time " data-hour="15" data-minute="0">15:00</div><div class="xdsoft_time " data-hour="16" data-minute="0">16:00</div><div class="xdsoft_time " data-hour="17" data-minute="0">17:00</div><div class="xdsoft_time " data-hour="18" data-minute="0">18:00</div><div class="xdsoft_time xdsoft_current" data-hour="19" data-minute="0">19:00</div><div class="xdsoft_time " data-hour="20" data-minute="0">20:00</div><div class="xdsoft_time " data-hour="21" data-minute="0">21:00</div><div class="xdsoft_time " data-hour="22" data-minute="0">22:00</div><div class="xdsoft_time " data-hour="23" data-minute="0">23:00</div></div><div class="xdsoft_scrollbar"><div class="xdsoft_scroller" style="display: block; height: 10px; margin-top: 0px;"></div></div></div><button type="button" class="xdsoft_next"></button></div></div></body></html>